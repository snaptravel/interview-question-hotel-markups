const hotel_data = [
  {
      "hotel_id": 100,
      "hotel_name": "The Colorado Overlook Hotel",
      "supplier_price": 100,
      "supplier": "supplierA",
  },
  {
      "hotel_id": 150,
      "hotel_name": "The Pennsylvanian Factory Hotel",
      "supplier_price": 200,
      "supplier": "supplierB",
  },
]


const markup_info = [
  {
      "supplier_name": "supplierA",
      "markup_percentage": 10,
      "should_penalize": true,
  },
  {
      "supplier_name": "supplierB",
      "markup_percentage": 25,
      "should_penalize": false,
  },
]


const rule1 = {
  "1": {
      "conditionName": "hotel_id",
      "conditionComparison": "notEquals",
      "conditionValue": 100,
  }
}
const rule2 = {
  "1": {
      "conditionName": "supplier_price",
      "conditionComparison": "lessThan",
      "conditionValue": 200,
  },
  "2": {
      "conditionName": "hotel_id",
      "conditionComparison": "lessThan",
      "conditionValue": 200,
  },
}
const rule3 = {
  "1": {
      "conditionName": "supplier_price",
      "conditionComparison": "greaterThan",
      "conditionValue": 500,
  },
  "2": {
      "conditionName": "hotel_id",
      "conditionComparison": "greaterThan",
      "conditionValue": 300,
  },
}
const ruleset = [rule1, rule2, rule3]