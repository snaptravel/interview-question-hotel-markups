import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {

  public static void main(String[] args) {

    // Solution data.
    
    List<HotelPrice> HOTEL_PRICES = new ArrayList<>(Arrays.asList(
      new HotelPrice(100, "The Colorado Overlook Hotel", 100, "supplierA"),
      new HotelPrice(150, "The Pennsylvanian Factory Hotel", 200, "supplierB")
    ));

    List<SupplierMarkup> MARKUP_INFO = new ArrayList<>(Arrays.asList(
      new SupplierMarkup("supplierA", 10, true),
      new SupplierMarkup("supplierB", 25, false)
    ));

    Rule RULE_1 = new Rule(
      new ArrayList<>(Arrays.asList(
        new RuleCondition("hotel_id", "notEquals", 100)
      ))
    );

    Rule RULE_2 = new Rule(
      new ArrayList<>(Arrays.asList(
        new RuleCondition("supplier_price", "lessThan", 200),
        new RuleCondition("hotel_id", "lessThan", 200)
      ))
    );

    Rule RULE_3 = new Rule(
      new ArrayList<>(Arrays.asList(
        new RuleCondition("supplier_price", "greaterThan", 500),
        new RuleCondition("hotel_id", "greaterThan", 300)
      ))
    );

    List<Rule> RULE_SET = new ArrayList<>(Arrays.asList(
      RULE_1, RULE_2, RULE_3
    ));

    // Your solution here...
    System.out.println("Your solution begins here...");
  }
}

class HotelPrice {
  public int id;
  public String name;
  public String supplierName;
  public double sellingPrice;
  public double supplierPrice;

  public HotelPrice(int id, String name, double supplierPrice, String supplierName) {
    this.id = id;
    this.name = name;
    this.supplierPrice = supplierPrice;
    this.supplierName = supplierName;
  }
}

class SupplierMarkup {
  public String supplierName;
  public double markupPercentage;
  public boolean shouldPenalize;

  public SupplierMarkup(String supplierName, double markupPercentage, boolean shouldPenalize) {
    this.supplierName = supplierName;
    this.markupPercentage = markupPercentage;
    this.shouldPenalize = shouldPenalize;
  }
}

class RuleCondition {
  public String conditionName;
  public String conditionComparison;
  public double conditionValue;

  public RuleCondition(String conditionName, String conditionComparison, double conditionValue) {
    this.conditionName = conditionName;
    this.conditionComparison = conditionComparison;
    this.conditionValue = conditionValue;
  }
}

class Rule {
  public List<RuleCondition> ruleConditions;

  public Rule(List<RuleCondition> ruleConditions) {
    this.ruleConditions = ruleConditions;
  }
}


